package hibernate.xml19;

import java.util.Iterator;
import java.util.List;


import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class NameQueryTest extends TestCase {

	/**
	 * 外置命名查询
	 */
	public void testQuery1() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			List students=session.getNamedQuery("searchStudents")
						.setParameter(0, 10)
						.list();
			for(Iterator iter=students.iterator();iter.hasNext();){
				Student student= (Student) iter.next();
				System.out.println("name="+student.getName());
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
}
