package hibernate.xml19;

import java.util.Iterator;
import java.util.List;


import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class SimpleObjectQueryTest1 extends TestCase {

	/**
	 * 实体对象查询
	 */
	public void testQuery1() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			//返回Student对象的集合
			//可以忽略select
			List students=session.createQuery("from Student").list();
			for(Iterator iter=students.iterator();iter.hasNext();){
				Student student= (Student) iter.next();
				System.out.println("name="+student.getName());
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
	/**
	 * 实体对象查询
	 */
	public void testQuery2() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			//返回Student对象的集合
			//可以忽略select,表可以使用别名
			List students=session.createQuery("from Student s").list();
			for(Iterator iter=students.iterator();iter.hasNext();){
				Student student= (Student) iter.next();
				System.out.println("name="+student.getName());
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
	
	/**
	 * 实体对象查询
	 */
	public void testQuery3() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			//返回Student对象的集合
			//可以忽略select,表可以使用as命名别名
			List students=session.createQuery("from Student as s").list();
			for(Iterator iter=students.iterator();iter.hasNext();){
				Student student= (Student) iter.next();
				System.out.println("name="+student.getName());
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
	
	/**
	 * 实体对象查询
	 */
	public void testQuery4() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			//返回Student对象的集合
			//使用select查询实体对象,必须使用别名
			List students=session.createQuery("select s from Student as s").list();
			for(Iterator iter=students.iterator();iter.hasNext();){
				Student student= (Student) iter.next();
				System.out.println("name="+student.getName());
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
	
	/**
	 * 实体对象查询
	 */
	public void testQuery5() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			//不支持select * from ...这样的查询语句
			List students=session.createQuery("select * from Student").list();
			for(Iterator iter=students.iterator();iter.hasNext();){
				Student student= (Student) iter.next();
				System.out.println("name="+student.getName());
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
}
