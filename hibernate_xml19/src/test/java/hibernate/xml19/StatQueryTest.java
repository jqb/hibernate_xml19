package hibernate.xml19;

import java.util.Iterator;
import java.util.List;


import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class StatQueryTest extends TestCase {

	
	/**
	 * 统计查询
	 */
	public void testQuery1() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			
//			List students=session.createQuery("select count(*) from Student").list();
//			Long count=(Long)students.get(0);
//			System.out.println("count="+count);
			Long count=(Long)session.createQuery("select count(*) from Student").uniqueResult();
			System.out.println("count="+count);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
	
	public void testQuery2() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			List students=session.createQuery("select c.name,count(s) from Student s join s.classes c group by c.name order by c.name").list();
			for(Iterator iter=students.iterator();iter.hasNext();){
				Object[] obj=(Object[])iter.next();
				System.out.println("c.name"+obj[0]+",count="+obj[1]);
			}			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
}
