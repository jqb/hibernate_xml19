package hibernate.xml19;

import org.hibernate.Session;
import org.hibernate.Transaction;

import junit.framework.TestCase;

public class DMLQueryTest extends TestCase {
	public void testQuery1() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			session.createQuery("update Student s set s.name=? where s.id<?")
								.setParameter(0, "aaa")
								.setParameter(1, 5)
								.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
}
