package hibernate.xml19;

import java.util.Iterator;
import java.util.List;

import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class SqlQueryTest extends TestCase {

	/**
	 * 原生sql测试
	 */
	public void testQuery1() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			// 可以拼字符串
			List students = session.createSQLQuery("select * from xml19_student").list();
			for (Iterator iter = students.iterator(); iter.hasNext();) {
				Object[] obj = (Object[]) iter.next();
				System.out.println("id=" + obj[0] + ",name=" + obj[1]);
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
	
}
