package hibernate.xml19;

import java.util.Iterator;
import java.util.List;


import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class ObjectNavQueryTest extends TestCase {

	/**
	 * 对象导航查询
	 */
	public void testQuery1() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			
			List students=session.createQuery("select s.name from Student s where s.classes.name like '%1%'").list();
			for(Iterator iter=students.iterator();iter.hasNext();){
				String name= (String) iter.next();
				System.out.println("name="+name);
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
}
