package hibernate.xml19;

import java.util.Iterator;
import java.util.List;


import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class JoinQueryTest extends TestCase {

	
	/**
	 * 连接查询
	 */
	public void testQuery1() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			
			List students=session.createQuery("select c.name, s.name from Student s join s.classes c").list();
			for(Iterator iter=students.iterator();iter.hasNext();){
				Object[] obj= (Object[]) iter.next();
				System.out.println("id="+obj[0]+",name="+obj[1]);
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
	
	
	/**
	 * 连接查询
	 */
	public void testQuery2() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			
			List students=session.createQuery("select c.name, s.name from Classes c left join c.students s").list();
			for(Iterator iter=students.iterator();iter.hasNext();){
				Object[] obj= (Object[]) iter.next();
				System.out.println("id="+obj[0]+",name="+obj[1]);
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
	
	/**
	 * 连接查询
	 */
	public void testQuery3() {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			
			List students=session.createQuery("select c.name, s.name from Classes c right join c.students s").list();
			for(Iterator iter=students.iterator();iter.hasNext();){
				Object[] obj= (Object[]) iter.next();
				System.out.println("id="+obj[0]+",name="+obj[1]);
			}
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			HibernateUtil.closeSession(session);
		}
	}
}
